---
title: "Arrêtés de mise en demeure concernant des carriers"
date: 2024-05-27T0:45:00+02:00
notoc: true
---

En mai 2024, la Préfecture de l'Ariège a publié deux arrêtés de mise en demeure concernant des carriers.


Ce n'est qu'une petite victoire, mais cela nous conforte dans notre volonté de nous opposer à la surexploitation du granulat en Ariège.

Voici les extraits des arrétés :

**La société Denjean Ariège Granulats**

*"l’exploitant n’a pas été en mesure de justifier de l’absence d’impact sur la qualité des
sols du fait de l’acceptation de chargements de déchets inertes sans document
d’acceptation préalable valide"*

*"ces manquements sont susceptibles de provoquer des pollutions de sols ou
des eaux et conditionnent le respect de la sécurité et de la salubrité publiques"*

**La société Sablières Malet**

*"l’exploitant n’a pas été en mesure de justifier d’une aire étanche de ravitaillement et
d’entretien des engins de chantiers fonctionnelle, de la récupération totale des eaux ou
des liquides résiduels, et de la présence d’un caniveau en périphérie de cette aire"*

*"l’exploitant n’a pas été en mesure de justifier du contrôle visuel des déchets à l’entrée
de la carrière et lors du déchargement du camion afin de vérifier l’absence de déchets
non autorisés"* 

*"l’exploitant n’a pas été en mesure de justifier que des déchets d’enrobés bitumineux
relevant du code 17 03 02 de la liste des déchets figurant à l’annexe II de l’article
R. 541.8 du code l’environnement on fait l’objet d’un test montrant qu’ils ne
contiennent ni goudron ni amiante"*

*"l’exploitant n’a pas respecté l’origine des sites d’accueil prévus pour l’acceptation des
déchets inertes sur le site de la carrière, en acceptant directement sur la carrière des
déchets issus d’un chantiers"*

[Les arrêtés sur le site de la préfecture](https://www.ariege.gouv.fr/contenu/telechargement/29825/202472/file/recueil-09-2024-044-recueil-des-actes-administratifs-1.pdf)


