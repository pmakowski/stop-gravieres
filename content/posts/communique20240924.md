---
title: "Communiqué du 23 septembre 2024"
date: 2024-09-24T0:45:00+02:00
notoc: true
---

Les associations ariégeoises déposent un recours contre le SRCO auprès du Tribunal administratif.

A la suite de l'adoption par le Préfet de la Région Occitanie du Schéma Régional des
Carrières d'Occitanie (SRCO), par arrêté en date du 16 février 2024, nos associations ont déposé un recours gracieux auprès de celui-ci le 12 avril dernier, tendant au retrait ou à la modification du SRCO.

Ce recours préalable n'ayant reçu aucune réponse dans les délais règlementaires, les
associations APRA Le Chabot, Comité Ecologique Ariégeois, France Nature
Environnement Occitanie-Pyrénées, avec le soutien d'Eau Secours 31, ont décidé de
poursuivre leur intervention en déposant un recours auprès des instances juridiques
administratives avant la date butoir du 13 août 2024.

Le SRCO, tel qu'il est formulé actuellement, est en effet de nature à porter atteinte de manière grave à l'environnement, à l'eau et aux milieux aquatiques, aux paysages, aux sites naturels. Ce schéma Régional est également incohérent dans la perspective du
réchauffement climatique et de l'économie de la ressource en matériaux.

Cette action juridique est dans la suite logique des différentes actions et interventions du Collectif Stop Gravières et des associations qui le composent, pour faire reconnaître les effets destructeurs sur la nappe phréatique de la vallée de la basse Ariège et sur l'environnement des carrières alluvionnaire

