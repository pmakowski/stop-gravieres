---
title: "Rencontre-débat"
date: 2024-10-03T08:51:22+02:00
notoc: true
---

## Rencontre-débat avec des membres du Collectif Stop Gravières et d’Eau Secours 31:

**Mercredi 27 novembre à 20h à l’Espace Lafage, 2 Rue Lafage (à 50m du Tram Croix de Pierre) 31300 Toulouse**

**L'eau et les gravières**

L'intensification de l'exploitation des sables et des galets alluvionnaires de la Basse-Ariège, aux mains de grands groupes du BTP, a de nombreux effets. L'extraction massive de ces matériaux naturels non renouvelables conduit à la destruction de terres agricoles de qualité, de leur environnement paysager, et d'une bonne part de la biodiversité. L'exploitation jusqu'au socle rocheux fait disparaître le filtre naturel de la nappe phréatique et laisse l'eau à nu, ouvrant la porte aux pollutions et à une évaporation massive.

Enfin, l'enfouissement de déchets du BTP pour remblayer une partie des carrières nuit à la circulation de l’eau et la pollue. Or l'eau de la nappe est en relation avec la rivière, et à terme elle constitue une part importante de l'eau bue par les Toulousain.e.s !

Nous sommes donc toutes et tous concernés !


![Flyer](Flyer_Reunion.png)

