---
title: "courrier concernant les gravières de la vallée de la Basse Ariège et Mirepoix"
date: 2025-01-20T14:00:00+02:00
notoc: true
---

Monsieur le Préfet,

À la suite de la réunion du premier octobre 2024, de nombreux problèmes ont été mis sur la table concernant l’extraction des graves alluvionnaires en Ariège. Cette concertation, la première du genre, étant donné la place et la parole données aux associations, doit, de notre point de vue, être le point de départ de rapports transparents et suivis entre l’administration et les citoyennes et les citoyens.

Nous souhaiterions recevoir le compte-rendu de cette réunion, qui à ce jour – sauf erreur de notre part – n’est pas parvenu aux organisations participantes.

Nous souhaiterions également avoir connaissance des analyses inopinées de la qualité de l’eau conduites à la demande de vos services sur l’ensemble des exploitations de Saverdun-Montaut et les suites relatives à la pollution de la nappe dont nous voudrions connaitre l’étendue et les moyens mis en place par les carriers ainsi que les mesures de protection de la population. 

Nous souhaiterions enfin connaître les suites des mises en demeure des entreprises, Malet et Denjean concernant les écarts concernant la règlementation sur l’enfouissement des déchets dans l’eau et la gestion des eaux usées de l’activité. Les carriers sont-ils désormais en conformité ?

Enfin, pourrions-nous connaître la superficie actuelle de mise à nu de la nappe, la quantité connue de déchets du BTP enfouis et donc la surface réelle restituée pour l’agriculture. 

Pour terminer, nous voulions vous faire part de notre inquiétude, et les associations présentes le premier octobre 2024 l’ont exprimé, sur l’intensification et les méthodes d’extraction des granulats dans la vallée de la Basse Ariège, le SRCO ayant été largement critiqué par les collectivités locales, des établissements publics, et les associations dont 4 ont déposé un recours en modification. 

Concernant l’enfouissement des déchets du BTP. Nous l’avons vu, sa mise en place a montré des lacunes dans la mise en œuvre et dans les contrôles depuis 2011. Cela a pour conséquence la pollution de la nappe – déjà démontrée par les analyses du Chabot – mais c’est aussi une bombe à retardement, sachant que des déchets non inertes y ont été enfouis et que le béton n’est pas inerte dans l’eau. CMGO développe sa propre filière de recyclage et n’enfouit plus de béton dans la nappe. Cette information nous a été donnée lors de la dernière commission de suivi à Saverdun. Donc, c’est dans cette voie que les carriers doivent s’engager. Ils doivent être accompagnés dans cette démarche et des obligations nouvelles pourraient être imposées.

Lors de nos échanges, la région par l’intermédiaire de son représentant a affirmé que la politique future est de ne plus extraire des sables et graviers dans les nappes phréatiques, voire dans les sédiments quaternaires. Cette orientation est plus que louable mais suppose une politique conséquente et ambitieuse concernant le recyclage et la sobriété. 

Pourtant, une autorisation préfectorale autorise l’extension de la carrière de Roumengoux avec une partie proche de l’Hers avec les conséquences que l’on connait (déconnection de la nappe, assèchement des milieux humides voisins…). Même si ce type d’exploitation n’est pas interdit par les textes, il est fortement recommandé d’éviter ces extractions. Les dommages peuvent alors être similaires à ceux de l’extraction des matériaux dans les lits mineurs. Enfin, l'exploitation est déjà quasi terminée sur les parcelles qui viennent d'être autorisées, ce qui ne nous semble pas conforme au droit. Aujourd’hui, il est urgent d’accompagner Mr Rescanières au vu des difficultés qu’il rencontre pour mettre en œuvre la réhabilitation de la gravière prévue par l’arrêté d’exploitation et ainsi retrouver de la biodiversité et limiter les effets néfastes de la gravière sur la dynamique fluviale. 

Par ailleurs, sur Roumengoux, un mètre de graves doit être laissé au fond, en vu de préserver la dynamique de la nappe. C’est plus que cela qu’il est nécessaire de préserver à minima, afin d’éviter des perturbations trop importantes de la dynamique de la nappe et ceci sur toutes les extractions en cours. Ce sujet est d’importance et ne doit en aucun cas être écarté de la réflexion de vos services. 

Dans l’attente d’une réponse à ce courrier et des documents demandés, nous vous prions de recevoir nos salutations les meilleures.

Les associations et organisations du collectif Stop Gravières

