---
title: "Une des plus importantes nappes phréatiques d'Occitanie gravement menacée par le BTP"
date: 2023-08-05T15:51:56+02:00
notoc: true
---

La ressource en eau en Occitanie est mise en péril par l'exploitation abusive de gravières sur une de ses plus importantes nappes phréatiques. Le Schéma Régional des Carrières prévoit d’entériner en 2023 la destruction de plus de 1000 ha de la nappe par les carrières exploitant dans l’eau (gravières), créant ainsi des giga-bassines à ciel ouvert, remblayées ensuite par des déchets du BTP.

Lire la suite dans le Club de Médiapart :  
[Une des plus importantes nappes phréatiques d'Occitanie gravement menacée par le BTP](https://blogs.mediapart.fr/bruan/blog/060323/une-des-plus-importantes-nappes-phreatiques-doccitanie-gravement-menacee-par-le-btp)

