---
title: "L'A69 et les gravières NO MACADAM - NO GRAVARAN"
date: 2023-10-19T14:51:22+02:00
notoc: true
---

Le projet de l'A69 Castres - Toulouse est en train de bétonner des terres fertiles, agricoles, des
zones humides, de détruire des arbres centenaires et des lieux de vie.

Ce projet politique écocide, Grand Projet Inutile et Imposé, a besoin de matières premières pour
exister et développer ses tentacules morbides.

Les gravières sont justement d'immenses trous creusés dans la terre jusqu'à atteindre la nappe
phréatique qui s'écoule dans des couches de granulats d'une qualité fine, convoités et accaparés par
les grands groupes industriels extracteurs, comme Lafarge, Denjean....

Construire cette autoroute, c'est s'accaparer la "nature" dans d'autres territoires.
En effet, les gravières de la Basse Ariège sont en partie destinées à l'A69. La quantité de
granulats nécessaire pour sa construction est astronomique : 2,6 millions de tonnes. Transportés
comment ? En camion évidemment !

En Basse Ariège, les pratiques des carriers sont elles aussi écocides et mortifères. Ils mettent la
nappe phréatique à nu et créent des bassins immenses ressemblant à des lacs. Cette eau que l'on voit
en traversant les villes de Pamiers, Saverdun, Montaut, Cintegabelle, est en fait la nappe
phréatique, qui diminue en quantité tout en s'évaporant. Pour contrer cela, les carriers
remblaient les bassins avec des déchets du BTP qui, au contact de l'eau, ne sont pas inertes et
polluent les terres agricoles autour et la nappe qui s'étend jusqu'à Toulouse ! La pollution est
chimique et bactériologique. Et cela en toute légalité : les déchets sont "valorisés", selon les
carriers et l'Etat !

Aujourd'hui, 240 ha de bassins sont exploités, 850 nouveaux ha sont prévus sur des terres
agricoles.. Tout part ailleurs : alors que sa consommation en graviers et granulats diminue, on
demande à l'Ariège d'augmenter sa production de 30% pour alimenter les régions déficitaires
(toulousaine, biterroise, montpellierienne...) et les grand projets inutiles (A69, Ligne TGV,
doublement du périphérique montpellierin...

Lafarge et ses copains tuent au nom de l'économie et défendent un monde en train de
péricliter de leurs propres mains. 
Notre lutte est impérieuse car elle concerne l'ensemble du monde vivant et rien ne peut
arrêter celleux qui l'ont compris.

Extinction Rebellion Foix a fait de la lutte contre les gravières et leur monde écocide son combat
principal. Nous ne lâcherons pas tant que le moratoire sur l'extension des gravières ne sera pas
remis, que le remblaiement avec les déchets de polluants du BTP ne sera pas stoppé et que
l'exploitation de la nappe phréatique ne sera pas reconsidérée.

Sans gravières, pas d'A69, et inversement.

Extinction Rebellion Foix

Pour rejoindre le canal d'information Telegram de La Gravière S'amuse (manif'action du 1er juillet
2023 contre les gravières) sur lequel trouver les prochaines actions : [t.me/infolagravieresamuse](https://t.me/s/infolagravieresamuse)

Sur l'A69 : [La Voie est libre](https://www.lvel.fr/)
