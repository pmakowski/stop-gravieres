---
title: "Conférence de presse 9 avril 2024"
date: 2024-04-09T08:51:22+02:00
---

## Recours gracieux à l'encontre du Schéma Régional des Carrières d'Occitanie (SRCO) Conférence de Presse du 9 avril 2024

[le communiqué en pdf](Conf-Presse-SRCO-9-4-24.pdf)

[La vidéo de la conférence](https://tube.fdn.fr/w/w9YjmPF93UQbC6qNBTKWav)


Par arrêté du 16 février 2024 publié au recueil des actes administratif Mr le Préfet de Région à  approuvé le Schéma Régional des Carrières d'Occitanie. Il choisi la consultation publique d'un mois par voie électronique pendant la période d'été, du 3 juillet au 7 aout 2023.
Cette modalité de concertation est totalement inadaptée, sur un sujet aussi important qu'un Schéma Régional des Carrières d'Occitanie qui engage la région pour les douze prochaines années et impactera près de six millions de citoyens sur son territoire. avec près de 5,924 millions d’habitants, la région s’affiche comme la 5e région la plus peuplée de France or le décompte des observations au terme de cette consultation, fait état d'à peine 255 observations recueillies soit 0,00430452 % des habitants concernés. pas même 1 pour 10 mille habitants !

Le SRCO s'installe en situation d’urgence écologique. Urgence climatique et urgence environnementale avec une chute vertigineuse de la biodiversité. Les impacts de ces bouleversements vont être de plus en plus prégnants au niveau de la santé, du pouvoir d’achat, de la stabilité politique et même de la survie de l’espèce humaine.
Aussi ce schéma doit répondre à ces urgences avec un engagement à 100% de toute la région pour assurer un avenir souhaitable et durable. Pour atteindre cet objectif, le SRCO devrait avoir comme fil conducteur notamment :

- une gestion sage et sobre de la ressource minérale ne générant aucune pollution,
- des entreprises fonctionnant en économie circulaire et produisant un minimum de déchets,
- des déchets du BTP à 100% recyclable conformément à la Directive Cadre Européenne sur les déchets,
- l'économie quantitative et la préservation qualitative de la ressource en eau,
- des transport des matériaux privilégiant la proximité ou le rail pour limiter au maximum les émissions de CO2,
- l'économie des surfaces agricoles et la limitation/réduction des surfaces artificialisées,
- l'adéquation avec les autres plans et schémas de même échelle ou de niveau supérieur.

Comme nous le relevons le SRCO, dans sa version actuelle, ne répond pas à ces impératifs. Loin s'en faut.

Aussi nos associations, le Chabot, FNE OP, CEA et Eau Secours 31 demandent au Préfet de Région de modifier ou abroger son arrêté d'approbation du SRCO.

### Un schéma résolument consumériste écrit sous la dictée de l'UNICEM
(Union Nationale des Industries de Carrières et Matériaux de construction)

Il invoque un taux de croissance de la population d'Occitanie de  0,62% pour justifier d'une consommation moyenne de matériaux de plus 8% avec un pic de 14.7 % en 2026 et engage un programme de grands travaux démesuré, générateur et encourageant les émissions de CO2.

- l'Ariège verra donc sa production de graves alluvionnaires, concentrée sur le secteur de la Basse Ariège, augmenter considérablement alors que sa consommation diminue.
- La pratique d'enfouissement des déchets du BTP continuera de polluer durablement la nappe pluviale et alluviale.
- En Occitanie l'acheminement des matériaux à près de 95 % par la route génèrerons des tonnes de CO2. Pour la seule Ariège ce seront plus ou moins, 50 000 rotations / an de camions aller retour pour 1 million de tonnes/an de matériaux acheminés vers les régions de consommation (agglomération Toulousaine essentiellement mais aussi les bassins voisins de Carcassonne et des Pyrénées Catalanes qui sont déficitaires). Ainsi les matériaux extraits dans le Comminges ou en Ariège pourront être utilisés pour l'aménagement de l'A69 ou de Port La Nouvelle.
- Le SRCO défie le SRADET  : Face aux enjeux nationaux d'artificialisation des sols, de perte des terres agricoles, de sobriété énergétique ou d'effondrement de la biodiversité, aucune mesure de sobriété n'est retenue. Ainsi 11 484 hectares artificialisés de carrières sont autorisées à l'exploitation en Occitanie. Plus de 1000 hectares sacrifiées en basse Ariège sur des zones agricoles bénéficiant d'équipements collectifs importants
- Le SRCO défie aussi le SDAGE Adour Garonne et les SAGE : enfouissement massif de déchets polluant les nappes, évaporation intense des lacs résiduels, affaiblissement des soutiens naturel d'étiages des cours d'eau... Une hérésie en période de crise climatique.

Nous souhaite voir modifier et/ou intégrer au Schéma Régional des Carrières d'Occitanie (SRCO) :

- des objectifs de sobriété induisant des économies conséquentes de production de matériaux issus des ressources primaires,
- la prolongation des moratoires existants sur l'ouverture de nouvelles carrière alluvionnaires et ne se limitant pas au autorisations existantes mais au niveau actuel de consommation,
- la définition des zones de chalandage de proximité ne dépassant pas 30 km,
- interdire la création d'une mesure donnant priorité d'accès aux gisements de granulats sur les PLU, PLUi et les SCOT
- des objectifs de recyclage conformes à la DCD (directive déchets) excluant la valorisation par enfouissement des déchets "inertes" du BTP et l'interdiction de l'enfouissement de ces déchets dans les nappes pluviales ou alluviales,
- l'intégration en zone rouge du SRCO des zones à enjeux environnementaux (Natura 2000, corridors écologiques, TVB répertoriées...), paysager, archéologiques,
- des obligations d'utiliser des transports alternatifs au tout routier chaque fois qu'il existe

Le recours gracieux n'est qu'une première étape, nos associations ne lâcherons pas et invitent tous ceux qui se sont prononcés contre ces orientations à les soutenir et les rejoindre.


