---
title: "Question orale au Sénat"
date: 2023-10-12T14:51:22+02:00
notoc: true
---

Question orale de Mme Raymonde Poncet Monge au ministre de la transition écologique et de la cohésion des territoires

Extension et enfouissement des gravières en Basse-Ariège
https://www.senat.fr/questions/base/2023/qSEQ23100839S.html

Le texte de la question :

Mme Raymonde Poncet Monge attire l'attention de M. le ministre de la transition écologique et de la cohésion des territoires sur les gravières en Basse-Ariège.

Depuis de nombreuses années les entreprises du bâtiment et travaux publics (BTP) multiplient les projets d'exploitation du granulat en Basse Ariège où se situe une nappe phréatique classée ressource prioritaire par le schéma directeur d'aménagement et de gestion des eaux (SDAGE) Adour-Garonne. Les carrières actuelles sont déjà à l'origine de nombreuses retenues d'eau, prélevée directement dans la nappe et qui en expose l'eau sur 250 hectares.

Pourtant, le schéma régional des carrières prévoit l'extension des carrières jusqu'en 2039 sur plus de 1 100 hectares afin d'alimenter des grands projets dont l'utilité, comme pour l'A69, est contestée à juste titre. Cette exposition des eaux de la grande nappe phréatique ariégeoise est criminelle puisqu'elle revient à l'assécher en l'exposant à l'évaporation. Selon une étude du bureau de recherches géologiques et minières (BRGM), réalisée avant les effets visibles du dérèglement climatique, 100 hectares d'eau exposée entrainent 1 000 000 m3 de déficit hydrique par an. Ainsi ce sont plus de 8 millions de mètres cube d'eau par an qui seront perdus avec l'extension des exploitations impactant toute la population et les milieux naturels, déjà contraints de subir des périodes de stress hydrique de plus en plus longues et importantes ! 

Il est urgent dans ce contexte de revenir sur l'extension des gravières et ce, d'autant plus, que le projet inclut également des autorisations d'enfouissement des déchets du BTP (en cohérence avec le code minier alors qu'interdit dans le code de l'environnement) déjà en cours dans la zone, alors même que ces derniers se dégrade en lixiviats chargés d'aluminium, rendant l'eau impropre à la consommation. Le risque est donc de polluer définitivement toute la nappe et de bloquer les ressources en eau d'une grande partie de la région alors même que les sécheresses se multiplient du fait de l'aggravation du dérèglement climatique. Elle lui demande dès lors de bien vouloir suspendre le projet d'extension des gravières, l'enfouissement des déchets du BTP dans la nappe et de le réévaluer à l'aune d'une étude sur les futurs besoins en eau des habitants et habitantes de la région Occitanie pour être à même d'affronter les effets du dérèglement climatique.
