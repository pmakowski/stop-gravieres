---
title: "Qui sommes nous ?"
notoc: true
---

![logo](/pages/logo-small.jpg)

## Le collectif

Le collectif Stop Gravières est né en mars 2023, dans le but de sauver la nappe phréatique de l'Ariège, d’arrêter le développement des carrières alluvionnaires de l’Ariège, de réduire les surfaces autorisées à l’exploitation (autorisées jusqu’en 2041 mais non encore exploitées) et de modifier leurs conditions d'exploitation pour minimiser autant que possible leurs impacts sur la ressource en Eau selon les recommandations des organismes publics à compétence reconnue en hydrogéologie par l'Etat. Il a également pour but d’harmoniser les conditions de production de matériaux de construction à travers toute l’Occitanie, en limitant tous les impacts sur la ressource en Eau.


Ce collectif a pour but de mieux informer la population sur les conséquences de l’implantation de carrières alluvionnaires sur la ressource en eau. Il peut interpeller les instances gouvernementales et locales en mutualisant les compétences de chaque membre du collectif.


Les décisions sont prises par consensus, chaque membre du collectif est bien sûr libre de prendre des initiatives en son nom propre.

## Objectifs

* Arrêt du déversement de déchets du BTP dans la nappe phréatique
* Ne plus creuser dans la nappe phréatique
* Limiter l'extraction en réservant les matériaux alluvionnaires aux besoins les plus nobles
* Développer le recyclage des déchets du BTP, privilégier la déconstruction et les remblais sur site à la démolition et à leur stockage dans la ressource en eau
* Prise en compte du changement climatique et des recommandations des hydrogéologues pour l'exploitation de tout gisement en Eau et pour l'implantation de nouvelles carrières ou de leurs extensions.



À l'heure actuelle les membres du collectif sont :

* [Apra - Le Chabot](https://www.apra-lechabot.fr/)
* [Aprova](http://protection-nappe-ariege.org) (Association pour la protection de la vallée de l'Ariège et de sa nappe phréatique)
* [CEA](https://cea09ecologie.org/) (Collectif Écologique Ariègeois)
* [La Confédération paysanne 09](https://ariege.confederationpaysanne.fr/)
* [Eau Secours 31](http://eausecours31.fr/)
* [Extinction Rebellion Foix et alentours](https://extinctionrebellion.fr/branches/foix-et-alentours/)
* [LDH Ariège](http://ldh-midi-pyrenees.org/les-sections/ariege/)
* [Les amis des soulèvement de la terre du Val d'Arac](https://lessoulevementsdelaterre.org/comites/la-carte-des-comites)


Vous pouvez vous [inscrire](https://www.ariege.eu.org/lists/?p=subscribe) à l'infolettre,
et nous contacter par courriel à : *stopgravieres AT ariege.eu.org*

