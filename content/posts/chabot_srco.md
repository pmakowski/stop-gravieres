---
title: "Le Chabot : Enquête Publique du Schéma Régional des Carrières d'Occitanie"
date: 2023-07-20T14:51:22+02:00
notoc: true
---

Résumé de la déposition de l'association Le Chabot à l'Enquête Publique du Schéma Régional des Carrières d'Occitanie (SRCO).

Notre association vous fait part de quelques éléments de réflexion pour
vous aider à comprendre les enjeux de ce schéma.


Avant tout nouveau projet de Schéma, le SRCO doit présenter un bilan
exhaustif des schémas départementaux lui servant de base.

Pourtant le bilan présenté se résume à la compilation des objectifs de
chacun des Schémas Départementaux des Carrières (SDC) sans aucune
analyse critique des résultats obtenus.

A titre d'exemple, pour le département de l'Ariège :

- est repris l'objectif de transport ferroviaire fixé à 50% des
matériaux exportés hors zone de proximité (le département 09) alors que
la réalité du fret mis sur le rail ne dépassait pas 10%. en 2017 ( base
retenue pour les analyses et prospectives et à peine 20% aujourd'hui)

- est repris l'objectif d'atteinte de 10% des matériaux issus du recyclage
alors que la réalité du recyclage est de l'ordre d'a peine 1%.

Ainsi le SRCO se base sur des chiffres erronés qu'il extrapole et étend
sur le 12 prochaines années.

Un schéma régional des carrières d’Occitanie climaticide

Qui se base sur :

- une consommation de matériaux de 42,36 Mt/an en hausse de 3 Mt/an soit
plus 8% par rapport à la consommation de 2017 (base retenue)

- une consommation moyenne par habitant 7,3 t/hab/an beaucoup plus forte
que la consommation moyenne métropolitaine de 5,2 t/hab/an (cette
projection de consommation ne sera pas remise en cause sur la durée du
schéma en 2031 (pas de clause de revoyure)

- un taux de croissance de la population d'Occitanie (0,73%) plus fort que
la moyenne métropolitaine (0,43%)

- un programme de grands travaux qui fait la part belle au TGV 53%, au tout
autoroutier 28% et au projet de Port La Nouvelle (16%), (seul le
développement du métro Toulousain nous semble vertueux).

- la disparition des moratoires départementaux sur l'ouverture de
nouvelles carrières comme nous avions pu l'obtenir en Ariège.

Les grands bassins de consommation sont déficitaires (Toulousain,
Biterrois, Montpellier...) et nécessitent des flux importants de
matériaux qui impliquent des transports couteux en CO2. L'Ariège par
exemple, avec un besoin en matériaux de 0,95 Mt/an, se voit dans
l'obligation d'augmenter sa production d'origine alluvionnaire à 90% des
matériaux extraits dans le département (de 1,4 à 1,8 Mt/an soit +29%),
pour alimenter la région Toulousaine extrêmement déficitaire (9,66Mt/an
consommé pour 1,70 Mt/an de production).

Le SRCO n'offre aucune garantie de réduction des distances parcourues par
les flux de matériaux, au contraire :

- il ne chiffre aucune économie de la ressource oubliant les nécessités
de sobriété,

- il entérine des flux interbassins sans limite de volumes ni de
distances,

- il ne répond pas à l'indisponibilité des lignes SNCF invoquée par les
carriers pour échapper à leur obligation de transport ferré,

- il ne chiffre aucune émission de CO2 invoquant un impact mineur sur le
climat et les émissions de gaz à effet de serre.

En étendant la possibilité d'enfouissement des déchets du BTP à toute
l'occitanie le SRCO

- face à l'enjeu national d'artificialisation des sols et à la perte des
terres agricoles aucune mesure de sobriété n'est retenue. Pourtant pour
le seul département de l'Ariège plus de 1 000 hectares de terres
agricoles (parmi les plus productives et ayant fait l'objet
d'investissements collectifs importants) ont déjà fait l'objet
d'autorisations d'extraction.

Il est urgent de réserver les matériaux nobles que sont les graves
alluvionnaires à des usages nobles (bande de roulement routier, bétons à
très fortes contraintes) et de préserver ainsi des sols inutilement
pillés.

En conclusion :

Notre association émet des observations très critiques sur un schéma
qu'elle estime consumériste, basé sur des grands projets inutiles et non
pertinents compte tenu des enjeux climatiques environnementaux du moment.

Concernant le département de l'Ariège :

* l'Ariège verra sa production de graves alluvionnaires, concentrée sur
le secteur de la Basse Ariège, augmenter considérablement de près de 30
% alors que sa consommation diminue.

* La pratique d'enfouissement des déchets du BTP n'est pas remise en cause
et continuera de polluer durablement la nappe pluviale et alluviale.

* L'acheminement de 1 million de tonnes/an de matériaux vers les régions
de consommation (agglomération Toulousaine essentiellement) à près de 80
% par la route génèrerons, plus ou moins, 50 000 rotations / an de
camions aller retour.

* La concentration des gravières et de leur développement se concentre
sur les meilleures terres agricoles du département. Déjà plus de 1000
hectares sacrifiées en basse Ariège sur des zones bénéficiant
d'équipements collectifs importants.

L'association "le Chabot souhaite voir modifier et/ou intégrer au Schéma
Régional des Carrières d'Occitanie (SRCO) :

* des objectifs de sobriété de consommation induisant des économies
conséquentes de production de matériaux issus des ressources primaires,

* la prolongation des moratoires existants sur l'ouverture de nouvelles
carrière alluvionnaires,

* la définition des zones de chalandage de proximité ne dépassant pas 30
km,

* interdire d'intégrer dans la mesure 1.4.1 (Préserver un accès aux
gisements d’intérêt national et régional identifiés par le schéma)
les gisements de granulats qu'ils soient d'origine alluvionnaire ou de
roche massive,

* des objectifs de recyclage conformes à la DCD (directive déchets)
excluant la valorisation par enfouissement des déchets "inertes" du BTP,

* l'interdiction de l'enfouissement des déchets "inertes" du BTP dans les
nappes pluviales ou alluviales,

* l'intégration en zone rouge du SRCO des zones à enjeux environnementaux
(Natura 2000, corridors écologiques, TVB répertoriées...), paysager,
archéologiques,

* des obligations, à chaque fois qu'il existe, d'utilisation de transports
alternatif au tout routier, des matériaux extraits, utilisés, recyclés,

* l'abandon des grands projets tels que les lignes TGV, le tout
autoroutier, l'extension de Port La Nouvelle et la réorientation des
projets vers : le fret ferroviaire de marchandises, le doublement des voies
ferrées principales, les infrastructures hospitalières, éducatives... et
limiter le routier au contournement des points noirs,

Pour ces raisons notre association est défavorable au projet de SRCO en
l'état.
