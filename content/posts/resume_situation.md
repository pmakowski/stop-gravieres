---
title: "Gravières en Ariège, un résumé de la situation"
date: 2023-08-20T0:45:00+02:00
---

*« Quand les puits s’assèchent,
l’eau devient grande richesse,
et l’abondance fuit le terroir ».
Proverbe*

## Préambule

Depuis les années 1970, 44 % des catastrophes climatiques sont liés à des
situations d’inondations, et il est estimé qu’aujourd’hui déjà, **la moitié de
l’humanité souffre du manque d’eau au moins un mois chaque année.**
La température moyenne de la France aujourd’hui est 1,7 C / 1900 soit bien
au-dessus de la hausse moyenne des températures à l’échelle mondiale
1,1 C en moyenne. De la Gironde aux Hautes-Pyrénées, la barre des 30 C,
dans de nombreuses villes, a été franchie. Les glaciers pyrénéens ont perdu
90% de leur superficie.
Il n’a pas plus ou quasi pas plu depuis mai en Ariège. 40% des cours d’eau :
chevelu, ru, ruisseaux et rivières sont à sec. Il n’y a plus d’eau, plus de vie.

**Ici dans les gravières, de la vallée de la Basse Ariège,
la situation est catastrophique.**

L’extractivisme démesuré tel qu’il est pratiqué en basse Ariège assèche ce
territoire tout cela pour continuer, comme avant, à alimenter des projets
climaticides. La basse vallée de l’Ariège est sacrifiée à une activité coupée du
réel et indifférente à ses effets destructeurs.

Pour comprendre ce qui se passe ici, nous avons créé ce document. Un
document d’information, de sensibilisation, pour toutes et tous,
permettant une lecture rapide mais aussi un approfondissement sur
la catastrophe écologique en vallée de Basse Ariège et pour
permettre une meilleure compréhension par toutes et tous de cette
problématique, mais aussi pour élargir nos soutiens.

**La nécessité absolue de défendre l'eau nous oblige à agir
pour préserver cette ressource indispensable.**

## En résumé

Les conséquences écologiques, économiques et bientôt humanitaires liées à
l’impact des gravières en Vallée d’Ariège sont synthétisées dans ce
document, qui se veut non exhaustif sur tous les aspects de la
problématique.

La nappe de la vallée de l’Ariège est un trésor géologique que nous a légué
la période glaciaire. Cette nappe abrite une ressource en Eau provenant des
Pyrénées, filtrée par les sables et les galets : l'une des plus pures de toute
l’Occitanie. Cette eau contribue au débit de la Garonne et à l’alimentation en
eau des toulousains.
Cette nappe de la vallée de l'Ariège est constituée à 85% de graviers et à
15% d'eau, recouverts d'une couche de terres arables. Ce sont les graviers
qui sont exploités et ce faisant, c’est la nappe et sa ressource en eau qui sont
détruites.

En Vallée de l’Ariège, c'est déjà 250 ha de terres agricoles détruites,
exploitées en gravières, transformées en déchetterie à ciel ouvert et en lacs
artificiels et bientôt 850 ha supplémentaires jusqu’en 2039-2041.
Ces projets sont menés par les entreprises Denjean Granulats, Spi
Batignolle, BGO&SECAM et Midi Pyrénées Granulats filière du groupe
Lafarge.

Les « trous d’eau » qui dégradent les nappes phréatiques dans les vallées de
l’Ariège et de la Garonne ont pour but l’extraction de matériaux destinés à
des projets principalement autoroutier et en dehors de l’Ariège.

Nous sommes en **état d’urgence sur l’eau**, le climat, la biodiversité. Les
carriers contribue à accélérer ces 3 phénomènes : l’assèchement de la nappe
et des sol, la disparition de la biodiversité et de par leur pratique ont un
impact fort en terme d’effet de serre.

**En Ariège, l'exploitation des carrières n'intègre pas les contraintes
environnementales actuelles.**

Au-delà de provoquer la baisse de la ressource en eau par évaporation et
rabattement de nappe, cette eau est polluée par l’enfouissement dans ces
"bassines" des déchets du Bâtiment et des Travaux public (BTP).Aujourd’hui 14 millions de tonnes de déchets sont dans la nappe provocant
déjà une grave pollution aux métaux tels fer et aluminium (600 fois le seuil
autorisés) et autres polluants issus du pétrole et des matériaux utilisés dans
le bâtiment, rendant l’eau impropre à la consommation.
Ces déchets permettent de "recyclés" les matériaux du BTP, simulée une
remise en état en rendant ses "sols" à l’agriculture et permettant au passage
de recevoir une commission substantiel pour les carriers.
Les conséquences économiques sont bien là pour le monde rural : des
restrictions d’eau qui impactent directement les riverains et les agriculteurs
avec pour ces derniers la baisse du rendement et de leurs revenus, mais
aussi la disparition d’exploitation (arboriculture, élevage de vaches
laitières…). Un territoire transformé durablement faisant fi du patrimoine et du
paysage.
Les cours d’eau qui approvisionnent plusieurs millions d’occitans sont
assoiffés et nécessitent des travaux colossaux pour sauver "un débit" dans
les rivières et pour permettre l’irrigation et maintenir l’eau au robinet.

**Aujourd’hui nous laissons déjà une vallée exsangue
où il y a pénurie d’eau**

Les générations futures ne comprendront pas pourquoi on les a privés d’eau,
la privation d’ailleurs sera probablement plus proche qu’on ne le pense.

Des associations de protection de l’environnement ariégeoise travaillent
depuis des années à alerter sur ce sujet et ont entrepris des démarches
juridiques pour limiter l’extraction et arrêter la mise à nu de la nappe et
interdire l’enfouissement des déchets du BTP dans la nappe. Elles ne sont
pas entendues.

Le collectif STOP GRAVIERES est né en mars 2023, dans le but de sauver la
nappe phréatique de l’Ariège, d’arrêter le développement des carrières
alluvionnaires de l’Ariège, de réduire les surfaces autorisées à l’exploitation
(autorisées jusqu’en 2041, mais non encore exploitées) et de modifier leurs
conditions d’exploitation pour minimiser autant que possible leurs impact sur
la ressource en eau selon les recommandations des organismes publiques à
compétence reconnue en hydrogéologie par l’Etat. Il a également pour but
d’harmoniser les conditions de production de matériaux de construction à
travers toute l’occitanie, en limitant tous les impacts sur la ressource en eau.
