---
title: "Communiqué et motion du 24 septembre 2023"
date: 2023-09-24T0:45:00+02:00
notoc: true
---

Au début de l’année 2023, le collectif STOP GRAVIÈRES a vu le jour, pour
répondre à l’urgence de mieux **communiquer** sur la menace qui pèse sur la nappe
phréatique et de relayer le travail de fond des associations de protection de
l’environnement d’Ariège et de Haute-Garonne, depuis plus de 10 ans.

Les 21, 22, 23 et 24 septembre, l’évènement «Au fil de l’eau» a concrétisé notre
volonté de donner plus de visibilité à ce problème. **Ces 4 jours sont une réussite.**

La protection de l’eau, c’est notre affaire à toutes et tous. Elle nécessite que la société
entière s’en préoccupe, que les décisions soient prises ensemble.

**L’eau est vitale, elle doit être partagée.**

Plus de 300 personnes ont assisté aux conférence de presse, conférences thématiques,
tables rondes et débats.

Le marche pour l’Eau a réuni 100 personnes.

La présence de la LDH, des élus.es nationaux et européens, des experts en sciences
sociales et écologiques a amplifié les apports concrêts des intervenants.tes des
associations qui constituent le collectif. Une dimension plus large a été apportée à la
problématique des gravières, en l’élargissant aux niveaux national et mondial et en
touchant non seulement, **la dramatique pollution de la nappe**, la destruction de
terres agricoles par les gravières, mais aussi la déstructuration des paysages et du
patrimoine. Tout un bouleversement de l’économie d’un territoire pour répondre à
des besoins en décalage avec **la nécessité de sobriété** et d’actions urgentes pour le
climat !

Les APNE (Associations de Protection de la Nature et de l’Environnement) ont étudié
le dossier du SRCO (schéma régional des carrières d’Occitanie), encore en cours de
validation et présenté à la consultation publique électronique. Elles ont émis des avis
défavorables tout en apportant des préconisations. Les collectivités ariégeoises, et
d’autres instances de la vallée de l’Ariège ont, elles-aussi, exprimé de fortes réserves.

Le collectif regrette ce calendrier imposé en plein milieu de l’été : **la démocratie exige l’écoute**, la
délibération transparente de l’ensemble des acteurs.trices, **l’étude sérieuse des voies
alternatives proposées par les élus.es et les citoyens.nes du territoire.** La
démocratie est bafouée dans un tel passage en force !

Ce schéma soutient une forte augmentation de l’extraction de granulats, déjà supérieure
à la moyenne nationale et l’Ariège verrait sa contribution augmenter considérablement.
Ce schéma doit respecter les accords de Paris sur le climat ainsi que la protection de
l’eau qu’imposent les différentes lois françaises et européennes.

Qualifier de «valorisation», le stockage des déchets dans la ressource en eau, **c’est une aberration écologique !**

**Le stockage des déchets, c’est hors de la nappe phréatique, hors des cours d’eau,
hors des plans d’eau, … pour préserver la santé.**

Il faut prendre en compte les préconisations des hydrogéologues pour anticiper sur la
gestion des nappes et non pas seulement les appeler à l’aide pour recharger des
nappes assoiffées, détournées à vaste échelle et polluées.

C’est pourquoi, après avoir fait ce travail d’information, nous continuerons. Les
propositions de l’élue nationale Anne Stambach-Terrenoir et de l’élu européen Benoît
Biteau, annoncées au cours de la conférence de presse du 21 septembre, porteront la
question des Gravières, à l’échelle nationale et européenne, en s’appuyant sur des
outils législatif et juridictionnel dont tout.e citoyen.ne peut s’emparer.

Il est temps de passer d’une réglementation ERC à une réglementation ERCA où A =
Adaptation à l’urgence du dérèglement climatique voire Annulation !

**Arrêtons «d’aménager» ou de «déménager» la ressource en Eau. Aujourd’hui,
en Occitanie, elle a surtout besoin d’être ménagée.**

La dégradation généralisée et accélérée des milieux et des territoires nous pousse à
agir. Il nous faut nous réunir pour réussir ensemble à ne pas aller vers une situation
où toute réparation sera impossible.

Nous demandons à ce que, suite aux préconisations portées lors de la consultation,
s’instaure un véritable dialogue, une réelle réflexion sur la modification du schéma.
Le SRCO participe de l’aménagement de nos territoires. Il nous concerne toutes et tous !
Le SRCO ne tient pas compte des rapports d’experts du BRGM et de l’INERIS,
des alertes des associations environnementales et du principe, constitutionnel,
de précaution.

**Ce SRC de l’Occitanie, c’est un suicide écologique et sanitaire prémédité !**

Ainsi, nous souhaitons rencontrer le Préfet de Région, la Présidente de Région, le
Maire de Toulouse et les élus.es des communes impactées, élus.es que nous n’avons
pas forcément rencontrés.es lors de cet évènement.

Notre société doit retrouver le goût du dialogue et de l’engagement trouver des
solutions pour respecter la quantité et la qualité des ressources pour les générations
futures, pour le vivant en retrouvant le sens de la sobriété.

**L’EAU, C’EST POUR LA VIE**  
**PAS POUR STOCKER LES DÉCHETS DU BTP !**  
**PAS POUR LES PROFITS**
