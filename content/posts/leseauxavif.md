---
title: "Les eaux à vif"
date: 2023-09-13T14:51:22+02:00
notoc: true
---

**[La Télé buissonnière](https://tele-buissonniere.org/) presente "Les eaux à vif"**

Gravières. Dans la basse vallée de l'Ariège ces carrières modifient notre environnement, impactent les eaux souterraines et les sols. Un essai visuel et sonore autour de l'extraction de sable et granulats, de l'atteinte aux paysages visibles et invisibles avec la parole d'un géomorphologue et d'un agriculteur. Des nappes phréatiques aux derniers vestiges du camp du Vernet-d'Ariège, une réflexion sur ce que cela nous inflige, à nous qui "mangeons" toustes de ce sable.

{{< vimeo_simple 840398095 >}}

