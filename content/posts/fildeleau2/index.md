---
title: "Au fil de l’Eau le programme"
date: 2023-09-14T14:51:22+02:00
---

## Stop Gravières présente : «Au fil de l’eau,de l’Ariège à Toulouse, une nappe pour la vie» les 21, 22, 23, 24 septembre 2023

4 jours pour un voyage au fil de l’Eau, un évènement ouvert et
constructif qui associera élus.es, scientifiques, syndicalistes,
agriculteurs.trices, citoyens.nes, pour parler de l’Eau, mieux connaître les impacts sur cette ressource et mieux la préserver
pour le futur.
La vallée de l’Ariège est l’un des derniers territoires de la France
qui subit un extractivisme démesuré des granulats alluvionnaires.
Et cela a de nombreuses conséquences écologiques et socio-économiques
néfastes : raréfaction et pollutions de la ressource en Eau,
dégradation et pollutions des Sols, destruction et renchérissement
accéléré des meilleures terres irriguées, dommages au paysage, ...

### "La flotte pour l’eau"

Jeudi 21 septembre à 13h30, départ à Saverdun, lieu-dit la minoterie de
"La flotte pour l’eau". Un voyage au fil de l’eau en canoë jusque Toulouse,
pour attirer l’attention sur le désastre écologique que constitue la
surexploitation des granulats dans la nappe phréatique de l’Ariège.

### Jeudi 21 septembre

**Pamiers, salle du hameau de Trémèges**

Comment préserver l’eau ? Que recouvre la notion de "commun" et en quoi peut-
elle être utile pour sauver la nappe phréatique de l’Ariège ?

* **16h00 : Accueil du public dans la salle**
Exposition, stand, infos, …
* **16h30 : Conférence de presse**
En présence de Benoît Biteau, agriculteur et député européen, d'Anne Stambach-Terrenoir, députée de Haute-Garonne et d’autres
représentants.es de la société civile et élus.es.
* **17h45 : Accueil par la municipalité**
* **18h00- 20h00 : Table ronde**
**Préservation de l’eau et reconnaissance de notre bien commun.
L’exemple de la nappe phréatique d’Ariège.**
Intervenants.tes : Benoît Biteau, agriculteur et député européen, Geneviève
Azam, universitaire économiste, Christian Morisse, LDH et Anne Stambach-Terrenoir, députée de Haute-Garonne.
Échanges avec le public.
* **20h20 : Film**
«De l’eau jaillit le feu», cinéma Le Rex, Pamiers.
Suivi d’un échange du collectif STOP GRAVIÈRES avec le public.

Ouverture du bar et petite restauration dès 16h

### Vendredi 22 septembre

**Saverdun, grande rue, salle des fêtes**
* **14h : Tour des gravières de Saverdun / Montaut** en voiture
Tout public, avec l’APROVA.
**Départ gare du Vernet d’ Ariège à 13h45**
* **16h : Accueil du public dans la salle**
Exposition, stand, infos, …
* **17h45 : Accueil par la municipalité**
* **18h00 : Conférence**
**Biodiversité des sols et rétention naturelle de l’eau**, par Jean Olivier,
docteur en écologie de l’Université de Toulouse, botaniste et biogéographe.
* **19h30 : Table ronde**
**Eau et agriculture/Sols vivants**
Intervenants.tes : Emmanuel Chemineau, paysan et formateur
en agroécologie, Jérôme Pédoussat, formateur agricole CFPPA Ariège Comminges,
Laurence Marandola, porte-parole nationale de la confédération paysanne,
Alex Franc, arboriculteur en vallée de l’Ariège.
Échanges avec le public

Ouverture du bar et petite restauration dès 16h

### Samedi 23 septembre

**Cintegabelle, place Jean Colombies, Salle de cinéma Gérard Philippe**

Qui représente le collectif STOP GRAVIÈRES né en mars 2023, dans le but de sauver la nappe phréatique de l’Ariège ?

* **14h : Tour des gravières de Cintegabelle** en voiture
Tout public, avec Cintegabelle en transition.
**Départ gare de Cintegabelle à 13h45**
* **16h : Accueil du public dans la salle**
Exposition, stand, infos, animations, …
* **16h45 : Accueil par la municipalité**, en présence de Jean-Louis REMY,
conseiller municipal
* **17h00-18h30 : Conférences**
**«Impacts des gravières à la nappe phréatique»**, par APROVA,
**«Analyse des conflits d’aménagement en France et négociation
environnementale»**, par Léa Sébastien, géographe de l’environnement.
* **19h : Table ronde**
**Collectif : STOP GRAVIÈRES**
Présentation par les associations de la problématique de l’extraction
du granulat et de l’enfouissement des déchets du BTP dans la nappe
phréatique de l’Ariège avec :
APRA-Le Chabot, Association de Protection des Rivières Ariégeoises, CEA
(Comité Ecologique Ariégeois), Confédération paysanne 09, APROVA
(Association de PROtection de la Vallée de l’Ariège), Extinction Rébellion Foix et environs,
Eau Secours 31, LDH 09 (Ligue des droits de l’Homme).
Échanges avec le public.
* **20h30 : Soirée conviviale**
Films courts, des documentaires, un film d’animation et une fiction sur l’eau.
Musique

Ouverture du bar et petite restauration dès 16h

### Dimanche 24 septembre

**Toulouse, île du Ramier**

* **Fin de matinée, rassemblement sur l’île du Ramier**
Prises de parole d’élus.es, scientifiques, syndicalistes, agriculteurs.trices,
citoyens.nes - Repas partagé
* **13h : Arrivée de la flotte pour l’eau**
Musique : Chorale …
* **14h : Marche pour l’eau**, au son de la Batucasol de Toulouse
vers l’hôtel de Région, la Préfecture et le Capitole
* **17h : Fin de l’évènement**

*Avec la participation des Associations «Pamiers Citoyenne» et «Cintegabelle en transition».
Merci aux militants.tes de Pamiers à Toulouse pour votre soutien.*

![affiche](fildeleau.jpg)

