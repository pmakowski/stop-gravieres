---
title: "Gravissimes gravières"
date: 2024-03-01T08:51:22+02:00
---

## Les AMI.E.S DES SOULÈVEMENTS DE LA TERRE du VAL D'ARAC vous proposent:

**Samedi 2 mars 14h30 à la mairie de Massat**

**GRAVISSIMES GRAVIÈRES**

Les carriers sont autorisés à creuser dans la nappe phréatique :

- Formant des giga-bassines qui s'évaporent.
- Déversant dans la nappe les déchets du BTP.
- Fournissant les soubassements à des projets écocides (autoroute A69, dique géante de Port-la-Nouvelle)

À l'origine cette plaine agricole fertile, est maintenant surexploitée, saccagée, polluée. Aujourd'hui 250 hectares de gravières, demain 1000 hectares, déjà autorisés par le préfet.

La circulation de la nappe phréatique est entravée, des cours d'eau disparaissent et l'eau est polluée.
En aval, elle se retrouve chargée de métaux lourds et de bactéries se deversant dans la Garonne.
Une eau qui alimente nos champs, nos villages et in fine l'agglomération de Toulouse.

Ne laissons pas détruire notre environnement. L'eau si vitales, doit rester un bien commn à la disposition des paysans et des paysannes, des citoyens et des citoyennes.


[le tract](tract.pdf)

