---
title: "Eau Secours 31 : Enquête Publique du Schéma Régional des Carrières d'Occitanie"
date: 2023-08-02T14:51:22+02:00
notoc: true
---

Avis d'Eau Secours 31 à transmis les éléments suivant à l'Enquête Publique du Schéma Régional des Carrières d'Occitanie (SRCO):

En préalable notre association est outrée que cette consultation ait
lieu en pleine période estivale sur une durée aussi courte au regard de
l'importance du sujet traité et du besoin d'appropriation des enjeux par
les populations concernées.

Pour ce qui concerne Eau Secours 31, l'état de la gestion des
carrières d'Ariège est un sujet particulièrement préoccupant pour notre
association dans la mesure où les usagers de l'eau de Toulouse et Toulouse
Métropole sont directement concernés tant s'agissant dans les temps à
venir de l'accès à une quantité d'eau vitale nécessaire que de la
qualité de cette eau.  
En effet le volume d'eau que consomment les populations de Toulouse et
de la Métropole provient pour 80% du total (chiffres cités dans le cadre
du SAGE) des eaux de surfaces prélevées dans l'Ariège.

Le schéma directeur des carrières prévoit que le département de
l'Ariège verra sa production de graves alluvionnaires, concentrée sur le
secteur de la Basse Ariège, augmenter considérablement, de près de 30 %.
Or lorsque le gravier a été extrait sur des hauteurs/profondeurs de
l'ordre de 16 à 20 m et que la nappe est atteinte, l'apparition de
véritables lacs donne lieu à un très puissante évaporation.

(Il faut citer ici le rapport du BRGM datant de 2014 dans une période
où l'urgence climatique n'était pas avérée et selon lequel
l'évaporation des nappes des gravières correspond à une moyenne de 150
litres /an et /habitant soit la consommation annuelle d'Eau potable de tous
les habitants de l'Ariège.)

Parallèlement l'enfouissement des déchets du BTP dans les nappes
pluviales ou alluviales de la Basse Ariège pour combler (partiellement)
les trous où sont prélevés les granulats de la nappe, pratiqué depuis
plusieurs années, n'est pas remis en cause dans le cadre du SRCO .

Or d'une part les déchets du bâtiment (notamment goudron, caoutchouc
etc.. )ne sont pas inertes, la qualité de l'eau qui en résulte et que
boivent et boiront les habitants de la métropole en est affectée, de
même le niveau de pollution des cultures maraîchères arrosées avec ces
eaux polluées et donc des aliments consommés, ce qui est problématique
du point de vue de la santé publique.

De plus cette pratique d'enfouissement continuera de polluer
durablement la nappe pluviale et alluviale.

L'enfouissement des déchets du BTP réduit également le bon
écoulement des eaux de la nappe . Il pourrait en résulter à terme une
réduction du volume d'eau de l'Ariège pouvant être prélevé pour
l’approvisionnement de la métropole notamment .

Il faut citer encore ce même rapport de 2014 . Il précise aussi, sans
prendre en compte les changements climatiques, que le débit de la nappe
phréatique vers la rivière Ariège qui alimente en eau potable la
Métropole toulousaine est très important en période d'étiage l'été et
que ce débit va aller en se réduisant de 50%...!!

L'un des problèmes qui en résulte également est celui du coût de la
dépollution. Ce coût est augmenté du fait de la baisse des débits mais
aussi de l'importance des substances polluantes. Or le principe du pollueur
payeur ne s'applique pas, qu'il s'agisse des agriculteurs utilisant
intensivement les pesticides qui rejoignent la nappe , ou des carriers qui
contribuent aux pollutions dues à la décomposition des déchets du BTP
(dit déchets inertes). Ces coûts sont externalisés et supportés par les
consommateurs avec la facture de l'Eau potable.

Enfin on notera qu’actuellement l'Hers ne coule plus et que l'eau du
lac de Montbel sert d'étiage pour l'Ariège !! Pour conclure il est donc
extrêmement dangereux de définir un nouveau Schéma directeur des
carrières sans tenir compte de la situation climatique et sans analyse
préalable de l'impact des décisions qui seront prises . Or il ne ressort
pas que ces impacts aient été analysés et que le schéma qui en résulte
en tienne compte . Notre association ne peut que donner un avis
défavorable au SCRO tel qu'il est.

Ci-après nos observations sur les différents documents concernés :

1. Bilan des Schémas départementaux  
Ce bilan fait apparaître des données incomplètes et ou imprécises
selon les départements aboutissant à une approche globale inexacte.

2. État des lieux et analyses des enjeux  
S'agissant de l'exploitation des carrières alluvionnaires pour
l'extraction des granulats, les données des opérations dans le BTP
s'arrêtent à l'année 2016, notamment sur la connaissance selon les
départements du nombre de constructions d'appartements engagée. De telles
données postérieures à 2016 méritaient d'être identifiées pour
justifier des volumes de constructions et des besoins en matériaux de
construction .
Parmi les éléments qui devraient se trouver dans l'état des lieux,
nous constatons l'absence d'éléments et de suivi du remblayage des
excavations par des déchets dits inertes; pour ceux ci d'ailleurs aucun
élément n'est apporté quant aux conséquences sur la qualité de l'eau
de leur dégradation dans l'eau ( pas d'analyses sur la présence de
métaux lourds par exemple)  
Parmi les enjeux, à aucun moment n'est cité l'enjeu climatique et de
ce point de vue les conséquences de l'exploitation des carrières
alluvionnaires notamment les conséquences aggravées de l'évaporation des
zones d'eau mises à nu ,de même que l'aggravation du déficit en eau .
De plus les enjeux environnementaux de niveau 1 ne sont ni identifiés
ni évalués.

3. Scénarios  
Il est incompréhensible que l'Ariège soit maintenue comme plus fort
exportateur de la région de granulats alluvionnaires sans explications ni
justifications. Pour mémoire, l'apport du fleuve Ariège constitue 80% de
l'eau prélevée pour la population de la métropole toulousaine . La
poursuite de l'exploitation très forte des carrières aggravera la
situation actuelle concernant le volume d'eau et la qualité de l'eau
servant pour les besoins domestiques, ou pour l'arrosage des produits
maraîchers.

4. Schéma régional des carrières :  
partie 3 orientations, objectifs et mesures:  
Ce schéma décline des intentions généreuses mais ne prévoit pas
d'éléments chiffrés sur le mesures de prévention liées à
l'exploitation des carrières. Le projet de "réduction progressive " des
impacts négatifs de cette exploitation n'est pas à la hauteur des enjeux
actuels , et notamment compte tenu du cadre lié au réchauffement
climatique.  
Aucune analyse du projet de SRC n' a été faite sur la compatibilité
avec les règles des SAGE et que les rapports des SAGE aient bien précisé
les nombreux aspects manquants. 
Aucune prise en compte de l'augmentation des volumes d'eau souterraine
du fait de l'exploitation profonde  
Aucune prise en compte de l'impact des carrières alluvionnaires sur
les zones humides et entraînant leur destruction temporaire à
définitive, et entraînant la mise en périodes zones d'alimentation de
ces zones humides.  
A noter que ce document ne contient pas l'information sur le fait que
toutes les collectivités du département d'Ariège ont émis un avis
défavorable au SCR

5. Rapport environnemental  
Aucune des réponses du rapport ne prend en compte l'urgence des
mesures à prendre face au dérèglement climatique

D'où notre avis avis défavorable.

