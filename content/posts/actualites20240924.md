---
title: "Gravières de la basse vallée de l’Ariège, actualités"
date: 2024-09-24T2:45:00+02:00
notoc: true
---

**Démarches juridiques, constats… Il est temps de stopper l’extension des gravières en Ariège.**

« Les alluvions de la plaine de l’Ariège et de l’Hers Vif constituent des
gisements préférentiels de sables et de graviers largement exploités […]
La présence de ces gravières dans un secteur où la surface
piézométrique de la nappe alluviale est proche de la surface
topographique génère le développement de plans d’eau dont la surface
correspond à celle de la nappe. » BRGM 2013
Suite aux constats alarmants sur l’état de la nappe, les associations de
protection de la nature se mobilisent depuis plus de 15 ans ; Les actions
s’amplifient et se diversifient depuis 2020 ; des actions d’envergure
comme « au fil de l’eau », « la gravière s’amuse », la gravière est une
ordure »… permettent de mettre le focus sur un des problèmes
environnementaux le plus préoccupant de la région.

**Des rapports de l’état qui montrent des manquements des carriers**

Nous avons obtenu les derniers rapports de contrôles sur les gravières
des services de la préfecture suite à notre saisine de la CADA. Nous
avons aussi recherché tous les contrôles effectués en 2022, 2023 et
2024 (Site Géorisque-IDSI). Il y a matière à réagir.

Suite à ces contrôles, la préfecture a mis en demeure les entreprises
Malet et Denjean de se mettre en conformité : les procédures
administratives et de contrôle à l’arrivée des camions de déchets du BTP
n’étant pas appliquées. Des contrôles de visu font état de la présence de
déchets non conformes, déjà observés par les associations (déchets
bitumeux, pneus, plastiques, polystyrène…). Il y a risque de pollution des
sols et de l’eau.

**Des risques avérés de pollution de l’eau et des sols !**

Depuis près de 10 ans, les exploitants stockent et enfouissent des
déchets du BTP. Pour ne parler que de l’aluminium, les dernières
analyses, dans les gravières Denjean, montrent la présence d’aluminium
dans les lacs 1, 2. La limite admissible est dépassée pour le lac N°3.
Plus simple, il n’y a pas de dosage de ce métal chez Malet ! CMGO (Carrières et Matériaux Grand-Ouest) est
en conformité.

Des analyses du Chabot (APRA-Le Chabot = Association de protection des Rivières Ariégeoise) et du CEA, faites par un labo agréé en 2016,
2017, 2018 et 2019, montrent la présence d’aluminium (dont une à des
taux 3 fois supérieurs à la norme de potabilité à l’aval des gravières).
L'aluminium ne fait pas partie du "Bruit de Fond" local. Ce taux très élevé
n’a rien d’étonnant : de nombreuses publications scientifiques et
institutionnelles démontrent que le processus de lixiviation des bétons
dans l’eau, dont le temps de dégradation est accéléré en présence de
nitrates (taux de nitrates moyen supérieur aux normes de potabilité de
50 mg/l dans la nappe phréatique de l’Ariège en 2024), produit de
l’aluminium.

**Une politique insensée et aujourd’hui contestée**

Les graves alluvionnaires de l’Ariège sont surexploitées et enrichissent
les multinationales du BTP et quelques exploitants agricoles qui vendent
leurs terres au détriment d’une vallée entière qui voit les puits s’assécher,
les ruisseaux disparaître, les sols assoiffés et le paysage se modifier
durablement. La prise de conscience sur la pollution provoquée par cette
activité est encore peu connue par les riverains. Dans les zones de
stockage des graves, nous constatons une accumulation. Extrait-on plus
que besoin ? Que fait-on avec ces granulats de très grande qualité ? Eh
bien, les entreprises du BTP sur fond de l’état réparent les routes et les
autoroutes pour faire passer des camions de plus en plus lourds (Accumuler du béton, tracer des routes – Nelo Magalhaes) ,
continuent à bétonner les abords des villes et à construire des
autoroutes en doublement de voies de circulation déjà bien aménagées
et de lignes de train. Ex : A69

Cette politique insensée est contestée par une grande partie de la
société, notamment des jeunes qui aujourd’hui mettent en jeu leur
intégrité physique pour sauver des écosystèmes dont nous toutes et tous
avons besoin. Une politique écocide qui semble bien mettre l’intérêt
particulier avant l’intérêt collectif. D’ailleurs, l’intérêt général aujourd’hui
est bien mis à mal par le nouveau projet de loi sur la simplification de la
vie économique qui limite les études d’impact et la consultation
citoyenne.

**La biodiversité s’effondre, le climat change… !**
**Le droit environnemental recule !**

Ainsi, Le Chabot, le CEA (Comité Ecologique ariégeois) et FNE-OP (Fédération régionale des associations de protection de la nature et de l'environnement) ont déposé un recours en
annulation contre le SRCO ce 3 septembre 2024. Les déchets
déposés dans les gravières pour les combler, donc dans la nappe ne
sont pas inertes et polluent durablement la nappe avec de l’aluminium,
des métaux lourds, des dérivés du pétrole… L’extension des gravières
provoque la baisse du niveau de la nappe et des modifications dans son
écoulement. Les besoins identifiés en granulats sont surévalués et
alimentent des projets d’un autre âge, inutiles, couteux, dévastateurs
dans un pays déjà bien impacté par l’activité humaine. Le SRCO, dans
sa version actuelle, ne permet pas de répondre à l’urgence climatique et
environnementale pour assurer un avenir souhaitable et durable. Il n’est
pas en adéquation avec les plans gouvernementaux supérieurs imposés
par la loi sur la transition écologique, loi qui s’impose à nous sous peine
de condamner les générations futures à vivre dans un monde ou
l’irréparable a été atteint.

L'enjeu est départemental - en effet la nappe phréatique de l'Ariège est
dramatiquement en danger - mais il est aussi national. La reprise du
nucléaire et autre grand projet autoroutier laisse penser que
l’extractivisme va connaître une recrudescence malgré la loi ZAN (Zéro Artificialisation Nette) ...
l'enfouissement des déchets se généralise avec le SRCO, mais aussi
dans d'autres régions, une aberration.

Bien sûr, nous ne gagnons pas toujours, les démarches juridiques sont
complexes, les tribunaux engorgés, les atteintes à l’environnement
décomplexées… Ainsi, les associations viennent de perdre au conseil
d’état - malgré le soutien du rapporteur public - après une procédure de
15 ans, contre l’arrêté d’extension de 150ha de Denjean du 19 juin 2009,
à Saverdun et tout ça sur des terres agricoles chèrement vendues !

Alors maintenant ? Eh bien nous ne baissons pas les bras. Les luttes
convergent pour proposer une « gestion » du monde plus respectueuse
de la nature et de l’humain.

Faute d’étude d’impacts et de contrôles approfondis, il apparait
nécessaire de créer un réseau de suivi quantitatif et qualitatif plus dense
pour évaluer l’impact des gravières actuelles et ainsi prévoir la
catastrophe à venir… Remettre à plat les besoins et les utilisations des
granulats ainsi que l’aménagement du territoire.

Pour cela, nous demandons un moratoire sur tout nouveau projet
d'extraction en Ariège. L’arrêt immédiat de l’enfouissement des
déchets. L’adaptation de l’industrie au défi environnemental de
notre planète doit être planifiée et accompagnée.

Le collectif Stop Gravières

