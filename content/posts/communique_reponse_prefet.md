---
title: "Communiqué réponse au Préfet"
date: 2023-10-01T14:51:22+02:00
notoc: true
---

Communiqué, droit de réponse des associations du collectif Stop Gravières

Alors que les élus ariégeois en basse Ariège rejettent unanimement l’extension des gravières et l’enfouissement des déchets du bâtiment dans leurs plans d’eau, directement reliés à la nappe phréatique, Monsieur le préfet de l’Ariège vient rétablir « la vérité sur les gravières ariégeoises » dans un communiqué du 25 septembre 2023, La Dépêche du Midi. Il revient sur leur étendue, les pollutions qu'elles provoquent et les contrôles qu'effectuent ses services.

Si vérité doit être rétablie il conviendrait que Mr le Préfet, qui vient juste de s'installer dans le département, consulte ses prédécesseurs qui ont donné les autorisations d'exploiter les gravières.

Car oui Mr le Préfet, c'est bien de plus de 1000 hectares de gravières qu'il s'agit. Vos services devraient pouvoir vous le confirmer. 100 hectares sont effectivement en exploitation sur le département, mais pour les prochaines années, sur la seule Basse Ariège, Denjean Cemex granulats détient déjà une autorisation de vos services d'exploiter 150 hectares, Midi Pyrénées Granulats Lafarge 201 hectares, Siadoux Colas 217 hectares, Mallet Spie Batignolles 76 hectares. Si vous y ajoutez Colas Varilhes et Rescanières à Roumengoux c'est bien près de 1000 ha qui sont actuellement en exploitation ou à venir.

Quant aux contrôles des exploitations et des matériaux enfouis dans la nappe, il est totalement illusoire et surréaliste de s'imaginer pouvoir contrôler une fois l'an les milliers de tonnes de déchets du BTP qui sont quotidiennement noyés dans la nappe et recouverts ensuite d'une couche de terre.

Pour preuve de la nocivité de ces pratiques et devant le déni constant des services de l'état et des carriers, nos associations ont, sur leur deniers, engagé pendant 4 années consécutives le Laboratoire Départemental agréé de Haute Garonne pour réaliser un suivi indépendant de la qualité de l'eau de la nappe en amont (point de référence) au droit et à l'aval des sites d'enfouissement. Les résultats sont sans appel et sont connus des services du Préfet, ils font ressortir l'apparition de pollutions aux métaux (fer, aluminium ...) et aux bactéries (streptocoques fécaux) rendant la nappe toxique et impropre à la consommation .

Nous nous tenons à la disposition de Mr le Préfet pour dissiper tout doute et malentendu sur ce dossier. 
