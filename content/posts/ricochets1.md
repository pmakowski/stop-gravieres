---
title: "Des ricochets en eau douce"
date: 2024-04-15T18:51:22+02:00
notoc: true
---

Roberto Della Torre prépare un documentaire «Des ricochets en eau douce» :

"Je vous invite à plonger dans la nappe phréatique de la basse Ariège qui s'étend de Pamiers à Toulouse.

Depuis une vingtaine d'années, l'extraction de graviers dans la nappe phéatique a pris des proportions industrielles au détriment de ce qui est considéré comme les meilleures terres agricoles du département de l'Ariège mais surtout de l'une des plus importante nappes phréatiques de la région.

Depuis plus de dix ans des associations environnementales, des maires, des agriculteurs alertent su la qualité de l'eau et sur les règles contradictoires du schéma régional des carrières, avec d'un coté dix ans d'enquêtes, de recours, d'études, de rencontres, d'attentes, de non réponse, de documents «confidentiels» et de l'autre coté, 10 ans de tentative d'extension des exploitations pour atteindre mille hectares.

Aujourd'hui, le regroupement d'associations actives sur ce sujet est épaulé par une nouvelle génération sensible à l'écologie et au changement climatique. Cette génération face aux injustices environnementales s'engagent avec des modalités nouvelles pour alerter la population et les pouvoirs publics sur la dangerosité des certains projets et sur la lenteur politico/législative.

Avec «Des ricochets en eau douce», je veux mettre en évidence la contradiction existante entre le développement économique à court terme qui génère des décisions essentiellement prises sous le prisme de l'intérêt monétaire et une vision «nouvelle» du développement, pour l'intérêt général.

Les plaignants demandent pour commencer à faire respecter les règles déjà écrites et en vigueur, mais peu appliquées et de nouvelles mesures permettant de meiux évaluer les impacts environnementaux des ces exploitations, de mettre en place des mesures de protection pour minimiser ces impacts.

La basse Ariège un cas d'école ?!"

Un projet porté par l'association La Trame.  
Pour plus d'information' : [La Trame](https://la-trame.org/des-ricochets-en-eau-douce/)
